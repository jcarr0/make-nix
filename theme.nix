/*
  Install any theme into a nix environment.
  Copy this to the location of the extracted theme archive and run `nix-env -if theme.nix`
*/
{ pkgs ? import <nixpkgs> {}}:
let
  currName = pkgs.lib.last (pkgs.lib.splitString "/" (builtins.toString ./.));
  mkTheme = name: src:
    pkgs.stdenv.mkDerivation {
      inherit name;
      src = src;
      buildCommand = ''
        copy_dirs() {
          root=$1;
          list=$2;

          for dir in $list; do
            found=$(find $src -name "$dir")
            echo "Found is $found at $root/$dir"
            if [ -a "$found" ]; then
              mkdir -p $out/share/$root
              cp -R $found $out/share/$root/
            fi
          done
        }

        copy_dirs "" "icons themes color-schemes"
        copy_dirs "plasma" "desktoptheme look-and-feel"
        copy_dirs "aurorae" "themes"
      '';
    };
in
{
  theme = mkTheme currName ./.;
}
